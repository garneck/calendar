import React from 'react'

import CalendarSummary from '../CalendarSummary'

import { ContentContainer } from './styled'

const App: React.FC = () => (
  <ContentContainer>
    <CalendarSummary />
  </ContentContainer>
)

export default App
