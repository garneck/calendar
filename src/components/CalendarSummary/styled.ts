import styled from 'styled-components'

export const Loading = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  width: 100%;
  height: 300px;
  border-radius: 5px;
  font-size: 30px;
  border: 1px solid ${(p) => p.theme.colors.border.light};
  background: ${(p) => p.theme.colors.background.light};
`

export const CalendarTableContainer = styled.div`
  border: 1px solid #bbb;
  border-radius: 5px;
`

export const RowContainer = styled.div`
  padding: ${(p) => p.theme.distances.m};
  display: flex;
  align-items: center;

  &:not(:last-child) {
    border-bottom: 1px solid ${(p) => p.theme.colors.border.light};
  }
  &:nth-child(odd) {
    background: ${(p) => p.theme.colors.background.light};
  }
  &:first-child {
    background: #000;
  }
`

export const Cell = styled.div`
  margin: 0 ${(p) => p.theme.distances.m};
  flex: 1;
  text-align: left;
  flex-basis: 15%;
  &:first-child {
    flex-basis: 20%;
  }
  &:last-child {
    flex-basis: 40%;
  }
`

export const HeaderCell = styled(Cell)`
  font-weight: bold;
  color: #fff;
`
