import React from 'react'

import CalendarTable from './components/CalendarTable'
import { Loading } from './styled'

import { getCalendarSummaryData } from './utils'
import { TCalendarEventSummariesData } from './interfaces'
import { DATA_DAY_SPAN } from './const'

const CalendarSummary: React.FunctionComponent = () => {
  const [
    eventSummaryData,
    setEventSummaryData,
  ] = React.useState<TCalendarEventSummariesData>()

  React.useEffect(() => {
    getCalendarSummaryData(
      DATA_DAY_SPAN
    ).then((data: TCalendarEventSummariesData) => setEventSummaryData(data))
  }, [])

  return (
    <React.Fragment>
      <h2>Calendar summary</h2>
      {eventSummaryData ? (
        <CalendarTable data={eventSummaryData} />
      ) : (
        <Loading>Loading event calendar...</Loading>
      )}
    </React.Fragment>
  )
}

export default CalendarSummary
