import {
  TCalendarEventSummary,
  TCalendarEventSummariesData,
} from './interfaces'
import { CalendarEvent, getCalendarEvents } from '../../api-client'

export const getCalendarSummaryData = (dayCount: number): any => {
  return getEventData(dayCount)
}

const getEventData = async (
  dayCount: number
): Promise<TCalendarEventSummariesData> => {
  const eventData: CalendarEvent[][] = []

  for (let i = 0; i < dayCount; i++) {
    const currentDayData = await getCalendarEvents(
      offsetDateByDays(new Date(), i)
    )
    eventData.push(currentDayData)
  }

  return {
    dailyEventSummaries: eventData.map((dailyEvents, i) =>
      summarizeEventData(dailyEvents, offsetDateByDays(new Date(), i))
    ),
    totalSummary: summarizeEventData(eventData.flat()),
  }
}

const summarizeEventData = (
  events: CalendarEvent[],
  date?: Date
): TCalendarEventSummary => ({
  totalEvents: events.length,
  totalDurationInMinutes: getTotalEventDurationInMinutes(events),
  longestEvent: getLongestEvent(events),
  date: date && formatDate(date),
})

const compareCalendarEventDurations = (
  a: CalendarEvent,
  b: CalendarEvent
): number => (a.durationInMinutes < b.durationInMinutes ? 1 : -1)

const getTotalEventDurationInMinutes = (events: CalendarEvent[]): number =>
  events
    .map((event) => event.durationInMinutes)
    .reduce((previous, current) => current + previous)

const getLongestEvent = (events: CalendarEvent[]): string =>
  events.sort(compareCalendarEventDurations)[0].title

export const formatDate = (date: Date): string =>
  `${date.getFullYear()}-${(date.getMonth() + 1)
    .toString()
    .padStart(2, '0')}-${date.getDate().toString().padStart(2, '0')}`

const offsetDateByDays = (date: Date, days: number): Date => {
  const result = new Date(Number(date))
  result.setDate(date.getDate() + days)
  return result
}
