export type TCalendarEventSummary = {
  totalEvents: number
  totalDurationInMinutes: number
  longestEvent: string
  date?: string
}

export type TCalendarEventSummariesData = {
  dailyEventSummaries: TCalendarEventSummary[]
  totalSummary: TCalendarEventSummary
}

export interface ICalendarTableRowProps {
  data: TCalendarEventSummary
}

export interface ICalendarTableProps {
  data: TCalendarEventSummariesData
}
