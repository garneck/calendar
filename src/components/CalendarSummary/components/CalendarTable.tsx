import React from 'react'

import CalendarTableRow from './CalendarTableRow'

import { ICalendarTableProps } from '../interfaces'
import { CalendarTableContainer, RowContainer, HeaderCell } from '../styled'

const CalendarTable: React.FC<ICalendarTableProps> = ({ data }) => {
  const { dailyEventSummaries, totalSummary } = data

  return (
    <CalendarTableContainer>
      <RowContainer>
        <HeaderCell>Date</HeaderCell>
        <HeaderCell>Number of events</HeaderCell>
        <HeaderCell>Total duration [min]</HeaderCell>
        <HeaderCell>Longest event</HeaderCell>
      </RowContainer>
      {dailyEventSummaries.map((data) => (
        <CalendarTableRow key={data.date?.toString()} data={data} />
      ))}
      <CalendarTableRow data={totalSummary} />
    </CalendarTableContainer>
  )
}

export default CalendarTable
