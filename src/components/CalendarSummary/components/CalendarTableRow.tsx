import React from 'react'

import { ICalendarTableRowProps } from '../interfaces'
import { RowContainer, Cell } from '../styled'

const CalendarTableRow: React.FC<ICalendarTableRowProps> = ({ data }) => {
  const { date, totalEvents, totalDurationInMinutes, longestEvent } = data

  return (
    <RowContainer>
      <Cell>{date || 'Total'}</Cell>
      <Cell>{totalEvents}</Cell>
      <Cell>{totalDurationInMinutes}</Cell>
      <Cell>{longestEvent}</Cell>
    </RowContainer>
  )
}

export default CalendarTableRow
