export const theme = {
  colors: {
    background: {
      light: '#dfe2e5',
    },
    border: {
      light: '#bbb',
    },
  },
  distances: {
    s: '8px',
    m: '16px',
    l: '24px',
  },
}
