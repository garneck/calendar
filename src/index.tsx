import React from 'react'
import ReactDOM from 'react-dom'
import { ThemeProvider } from 'styled-components'

import GlobalTheme from './css/GlobalTheme'
import { theme } from './css/theme'
import App from './components/App'

ReactDOM.render(
  <React.StrictMode>
    <ThemeProvider theme={theme}>
      <GlobalTheme />
      <App />
    </ThemeProvider>
  </React.StrictMode>,
  document.getElementById('root')
)
